@extends('layouts.app')

@section('content')
  <div class="container">
      <div class="justify-content-center">
        <a href="{{ url('/video/create') }}"><button type="button" class="btn btn-secondary btn-lg">Agregar Video</button></a>
        <br /><br />
        <div class="row">
          <div class="col">
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-md-8">
                    Listado de Videos
                  </div>
                </div>
              </div>
              <div class="card-body">
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">Name</th>
                      <th scope="col">Path</th>
                    </tr>
                  </thead>
                  <tbody class="tbody">
                    @foreach($videos as $key)  
                          <tr>
                            <td scope="row">{{ $key->name }}</td>
                            <td scope="row">{{ $key->path }}
                              <iframe width="340" height="260" src="http://www.youtube.com/watch?v=ODTgHFEdYcA?rel=0&amp;autoplay=0&amp;controls=1&amp;showinfo=1" frameborder="0"></iframe>
                              
                            </td>
                            <td>
                              <a href="/video/{{ $key->id }}/delete" id="{{ $key->id }}" class="a_delete">Eliminar |</a>
                              <a href="/video/{{ $key->id }}/edit" id="{{ $key->id }}" class="a_edit">Edit</a>
                            </td>
                          </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

      </div>
  </div>
@endsection