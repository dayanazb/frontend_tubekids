@extends('layouts.app')

@section('content')
    <body>
        <div class="container">
            <div class="row justify-content-center">
                <div class="col-sm-8">
                    <div class="card">
                        <div class="card-header">Formulario de Videos
                        </div>
                        <div class="card-body">
                            

                            {!! Form::open(['url' => '/video', 'method' => 'POST', 'enctype' => 'multipart/form-data']) !!}

                            <div class="form-group">
                                {!! Form::label('name', 'Name') !!}
                                {!! Form::text('name', '', ['placeholder' => '', 'class' => 'form-control', 'required']) !!}
                            </div>
                            <div class="form-group">
                                <label for="path">Path</label>
                                <div class="col-md-6">
                                <input type="file" class="form-control" name="path" >
                            </div>
                        </div>
                            {!! Form::submit('Agregar', ['class' => 'btn btn-info']) !!}

                        {!! Form::close() !!}

                        </div>

                    </div>
                </div>

            </div>
            
        </div>
@endsection