@extends('layouts.app')

@section('content')
    <div class="container">
        {!! Form::open(['url' => '/perfil/'.$perfil->id, 'method' => 'put']) !!}

        <div class="justify-content-center">
        	<div class="row">
        		<div class="col-md-2"></div>
          		<div class="col-md-8">
            		<div class="card">
	              		<h2>Editar perfil</h2>
              			<div class="card-header">
                			<div class="row">
                  				<div class="col-md-8">

                  					<div class="row">
							          <div class="col-md-4"></div>
							         	<div class="form-group col-md-4">
							            	<label for="name">Name:</label>
							            	<input type="text" class="form-control" name="name" value="{{$perfil->name}}">
							          	</div>
							          	<div class="form-group col-md-4">
							            	<label for="username">Username:</label>
							            	<input type="text" class="form-control" name="username" value="{{$perfil->username}}">
							          	</div>
							          	<div class="form-group col-md-4">
							            	<label for="pin">PIN:</label>
							            	<input type="text" class="form-control" name="pin" value="{{$perfil->pin}}">
							          	</div>
							          	<div class="form-group col-md-4">
							            	<label for="edad">Edad:</label>
							            	<input type="text" class="form-control" name="edad" value="{{$perfil->edad}}">
							          	</div>
							        </div>
							        <div class="row">
							          <div class="col-md-4"></div>
							          <div class="form-group col-md-4">
							            <button type="submit" class="btn btn-success">Actualizar</button>
							          </div>
							        </div>

                  				</div>
                  			</div>
                  		</div>
                  	</div>
                </div>
            </div>
        </div>

    {!! Form::close() !!}
    </div>
@endsection