@extends('layouts.app')

@section('content')
  <div class="container">
      <div class="justify-content-center">
        <a href="{{ url('/perfil/create') }}"><button type="button" class="btn btn-secondary btn-lg">Agregar Perfil</button></a>
        <br /><br />
        <div class="row">
          <div class="col">
            <div class="card">
              <div class="card-header">
                <div class="row">
                  <div class="col-md-8">
                    Listado de perfiles
                  </div>
                </div>
              </div>
              <div class="card-body">
                @if (session('status'))
                <div class="alert alert-success" role="alert">
                  {{ session('status') }}
                </div>
                @endif
                <table class="table">
                  <thead>
                    <tr>
                      <th scope="col">Name</th>
                      <th scope="col">Username</th>
                      <th scope="col">Pin</th>
                      <th scope="col">edad</th>
                    </tr>
                  </thead>
                  <tbody class="tbody">
                    @foreach($perfiles as $key)  
                          <tr>
                            <td scope="row">{{ $key->name }}</td>
                            <td scope="row">{{ $key->username }}</td>
                            <td scope="row">{{ $key->pin }}</td>
                            <td scope="row">{{ $key->edad }}</td>
                            <td>
                              <a href="/perfil/{{ $key->id }}/delete" id="{{ $key->id }}" class="a_delete">Eliminar |</a>
                              <a href="/perfil/{{ $key->id }}/edit" id="{{ $key->id }}" class="a_edit">Edit</a>
                            </td>
                          </tr>
                    @endforeach
                  </tbody>
                </table>
              </div>
            </div>
          </div>
        </div>

      </div>
  </div>
@endsection