<?php

/*
|--------------------------------------------------------------------------
| Web Routes
|--------------------------------------------------------------------------
|
| Here is where you can register web routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| contains the "web" middleware group. Now create something great!
|
*/

Route::get('/', function () {
    return view('welcome');
});

Route::post('register', 'UserController@store');
Route::post('auth', 'UserController@auth');
Route::get('index', 'UserController@index');
Route::post('video', 'VideoController@store');
Route::get('login', 'UserController@login');
Route::post('logout', 'UserController@logout');

Route::resource('perfil','PerfilController');

Route::get('dashboard','PerfilController@dashboard');

Route::get('/perfil/{id}/delete', 'PerfilController@destroy');


Route::get('/video/{id}/delete', 'VideoController@destroy');

Route::resource('video','VideoController');