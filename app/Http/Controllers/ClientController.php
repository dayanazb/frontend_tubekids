<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;
use GuzzleHttp\Client;

class ClientController extends Controller
{
    protected function registerUser($request)
  {
    //dd($request);
    $client = new Client();
    $response = $client->post('http://localhost:8000/api/register', [
        'form_params' => $request->all()
    ]);
    return $response->getBody()->getContents();
  }

  protected function loginUser($request)
  {
    $client = new Client();
    $response = $client->post('http://localhost:8000/api/auth/login', [
        'form_params' => $request->all()
    ]);
    return $response->getBody()->getContents();
  }

   protected function logoutUser()
  {
    $client = new Client();
    $response = $client->post('http://localhost:8000/api/auth/logout');
    return $response->getBody()->getContents();
  }

  /* Perfiles  */


  protected function createPerfil($request)
  {
    $client = new Client();
    $response = $client->post('http://localhost:8000/api/perfil', [
        'form_params' => $request->all()
    ]);
    return $response->getBody()->getContents();
  }
  protected function listPerfiles()
  {
    $request['token'] = session('token');
    $client = new Client();
    $response = $client->post('http://localhost:8000/api/lista_perfiles', [
          'form_params' => $request
    ]);
    return $response->getBody()->getContents();
  }

  protected function showPerfil($id)
  {
    $client = new Client();
    $response = $client->get('http://localhost:8000/api/perfil/'.$id);
    return $response->getBody()->getContents();
  }

  protected function updatePerfil($request)
  {
    $client = new Client();
    $response = $client->put('http://localhost:8000/api/perfil', [
        'form_params' => $request->all()
    ]);
    return $response->getBody()->getContents();
  }

  protected function deletePerfil($request)
  {
    $client = new Client();
    $response = $client->put('http://localhost:8000/api/perfil/'. $request['id'] . '/delete', [
        'form_params' => $request
    ]);
    return $response->getBody()->getContents();
  }


  /* Videos  */

  protected function createVideo($request)
  {
    $client = new Client();
    $response = $client->post('http://localhost:8000/api/video', [
        'form_params' => $request->all()
    ]);
    return $response->getBody()->getContents();
  }
  protected function listVideos()
  {

    $request['token'] = session('token');
    $client = new Client();
    $response = $client->post('http://localhost:8000/api/lista_videos', [
          'form_params' => $request
    ]);
    return $response->getBody()->getContents();
  }

  protected function showVideo($id)
  {
    $client = new Client();
    $response = $client->get('http://localhost:8000/api/video/'.$id);
    return $response->getBody()->getContents();
  }

  protected function updateVideo($request)
  {
    $client = new Client();
    $response = $client->put('http://localhost:8000/api/video', [
        'form_params' => $request->all()
    ]);
    return $response->getBody()->getContents();
  }

  protected function deleteVideo($request)
  {
    $client = new Client();
    $response = $client->put('http://localhost:8000/api/video/'. $request['id'] . '/delete', [
        'form_params' => $request
    ]);
    return $response->getBody()->getContents();
  }
}