<?php

namespace App\Http\Controllers;

use Illuminate\Http\Request;

class PerfilController extends ClientController
{
    /**
     * Display a listing of the resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function index()
    {
            $response = json_decode($this->listPerfiles());
            if (strval($response->code) == 200) {
                return view('crudPerfiles')->with('perfiles', $response->perfiles);
            }else {   
                flash($response->message)->warning();
                return back()->withInput();
            }
    }

    public function dashboard()
    {
        return view('dashboard');
    }

    /**
     * Show the form for creating a new resource.
     *
     * @return \Illuminate\Http\Response
     */
    public function create()
    {
        return view('perfiles');
    }

    /**
     * Store a newly created resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @return \Illuminate\Http\Response
     */
    public function store(Request $request)
    {
            $request['token'] = session('token');
            $response = json_decode($this->createPerfil($request));
            if (strval($response->code) == "201") {
                return $this->index();
            }else {   
                flash($response->message)->warning();
                return back()->withInput();
            }
    }

    /**
     * Display the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function show($id)
    {


    }

    /**
     * Show the form for editing the specified resource.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function edit($id)
    {
        $response = json_decode($this->showPerfil($id));
            if (strval($response->code) == 200) {
                return view('edit_perfil')->with('perfil', $response->perfil);
            }else {   
                flash($response->message)->warning();
                return back()->withInput();
            }
    }

    /**
     * Update the specified resource in storage.
     *
     * @param  \Illuminate\Http\Request  $request
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function update(Request $request, $id)
    {
        $request['token'] = session('token');
        $request['id'] = $id;
            $response = json_decode($this->updatePerfil($request));
            if (strval($response->code) == 200) {
                return $this->index();
            }else {   
                flash($response->message)->warning();
                return back()->withInput();
            }
    }

    /**
     * Remove the specified resource from storage.
     *
     * @param  int  $id
     * @return \Illuminate\Http\Response
     */
    public function destroy($id)
    {
        $request['token'] = session('token');
        $request['id'] = $id;
            $response = json_decode($this->deletePerfil($request));
            if (strval($response->code) == 200) {
                return $this->index();
            }else {   
                flash($response->message)->warning();
                return back()->withInput();
            }
    }
}
